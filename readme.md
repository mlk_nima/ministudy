# Ministudy

![alt tag](assets/icon/icon.ico)

* DB Accessible at assets/db/phpliteadmin.php

What is it?
-------
* This is a simple interface to gather ratings for a few songs.

UPDATES
==============
20.01.2017
----------
* Fixed the issue of multiple ratings for a single song by a single person.


19.01.2017
----------
* Sqlite DB instead of text file, featuring the complete user rating matrix in the table "ratings".  
* Each single rating is also stored in a separated table as it is being clicked.
* Included m4a files to support iPhone users.
* Star rating interface.
* Random selection of 20 songs, only among the "useful" songs.
* Floating submit button to remind the participant to push it. (No AJAX)

