
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mini Survey</title>
	<link rel="icon" href="assets/icon/icon.ico">
	<link rel="stylesheet" href="assets/css/mycss.css">
	<link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>	
	<script src="assets/sweetalert/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/sweetalert/sweetalert.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>

<body>

<form>

<h1>Thanks<br></h1>
<div align="center">
<div style="color:#4bc970"><i class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></i></div><br>
<em>We have received your ratings!</em>
</div>

</form>
</body>